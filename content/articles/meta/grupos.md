Title: Salas públicas no element e no telegram
Date: 2020-07-21

A principal motivação para a criação do hackerpace foi a vontade de criar mais espaços abertos que promovessem o compartilhamento de conhecimento entre estudantes (sobretudo da FEEC). Assim, visando criar esse ambiente de comunicação, criamos uma sala pública no element (antigo riot.im) e um espelho para telegram (ou seja, as mensagens enviadas em uma das salas aparecem também na outra e vice-versa)! Os links para participar de cada uma das salas são:

[sala do element/matrix](https://matrix.to/#/#hackerspacefeec:matrix.org)

[sala do telegram](https://t.me/joinchat/Ixtldxk1kIm6tmKfqzoiNw)

## Qual o objetivo da sala?

Como já dito, gostaríamos de prover um espaço de troca livre de conhecimento. Assim, achamos que o espaço pode ser um lugar propício para compartilhamento de links e projetos interessantes, debate sobre assuntos diversos e até mesmo propiciar encontros de pessoas com interesses em comum e que podem vir a produzir projetos conjuntamente. Também usaremos a sala para atualizar a comunidade sobre tudo o que ocorre relacionado ao hackerspace.

## Por que usar o element?

O cliente element - e o protocolo de comunicação matrix - constituem um mensageiro software livre e descentralizado que, ao nosso ver, é uma alternativa livre robusta a uma série de aplicativos proprietários e centralizados (slack, discord, whatsapp, etc.). Além disso, as salas privadas são encriptadas por padrão (e agora com um sistema simplificado de verificação de chaves), existem clientes para celular, desktop e navegador e está ativamente em desenvolvimento. Outra de suas características é a facilidade de integração com outros aplicativos (vide a sala espelhada no telegram). Assim, é a ferramenta de comunicação oficial escolhida para a organização do hackerspace.

## Por que usar o telegram?

Apesar de gostarmos mais do element como ferramenta de comunicação (e por isso ser nosso meio oficial), sabemos que a maior parte das pessoas não usam ele, seja por não conhecerem ou por qualquer outro motivo. Assim, uma solução de compromisso foi criar um bridge para uma sala de telegram que, além de ser uma alternativa livre (em oposição ao whatsapp, por exemplo), é mais usado e conhecido do que o element.

## O que mais está nos planos do hackerspace?

Nosso maior objetivo atualmente é conseguir um espaço físico, assim, estamos em diálogo com docentes e funcionários da FEEC a fim de estabelecer um espaço que beneficie a toda comunidade.
Já existem alguns conteúdos postados no site por membros do hackerspace, mas gostaríamos que fosse possível que qualquer estudante compartilhasse seu projeto. Atualmente conseguimos aceitar contribuições através de merge requests no gitlab ou email (esse procedimento será detalhado no próximo post). O próximo passo seria facilitar esse processo.
