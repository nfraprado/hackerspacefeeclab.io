title: Videoconferências sem o Google: Jitsi
tags: videoconferencia, google, jitsi, alternativa
date: 05-11-2020
author: Nícolas F. R. A. Prado

---

Devido à situação atual de quarentena, muitas atividades que antes eram feitas
presencialmente, agora estão tendo que ser feitas via vídeoconferências.
Em situações como essa, em que temos que nos adequar a um novo modo de
funcionamento, é comum utilizarmos aquela ferramenta com a qual já estamos
habituados.
A primeira ferramenta que surge à mente para realizar vídeoconferências e
resolver seus problemas geralmente é aquela que acabamos usando: Google Meets
(ou Zoom).

No entanto, não é porque uma alternativa é mais presente que é a única.
Temos uma opção menos conhecida, porém muito mais atraente: Jitsi.

O Jitsi é uma ferramenta de vídeoconferência como as demais, no entanto,
diferentemente das ferramentas anteriores, tem o propósito de oferecer a melhor
ferramenta possível, e não de lucrar vendendo anúncios usando seus dados.
Ele é também software livre, e portanto seu código é aberto, oferecendo maior
liberdade em seu uso.

Vantagens de se utilizar o Jitsi são:

-   o código aberto permite que a segurança da comunicação possa ser confirmada
    olhando-se o código-fonte;

-   pode ser utilizado sem a criação de nenhuma conta;

-   qualquer um pode rodar seu próprio servidor Jitsi sem qualquer custo;

-   qualquer um pode modificar seu código-fonte, tornando possível adicionar
    alguma funcionalidade necessária para seu uso particular;

Se a ideia de uma vídeoconferência segura, de qualidade, com privacidade, e sem
nenhum trabalho adicional te interessou, que tal organizar seu próximo encontro
usando o [Jitsi](https://meet.jit.si/)?

E sim, ele tem também aplicativos para Android e iOS, além da interface web.

Mais informações sobre a ferramenta podem ser encontradas no
[site](https://jitsi.org/).
